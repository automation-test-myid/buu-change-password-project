<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(         ( )   , - .            _     _538061</name>
   <tag></tag>
   <elementGuidId>f14ac6a2-252a-4539-97ad-e34b06ae4c11</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='alert-js']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>94d7be65-8d02-44df-966e-5db09c81d6dc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>2941cfee-7833-4382-bdb8-3ad0c473e7d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  '  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)</value>
      <webElementGuid>15f1c03e-61e5-41ce-966c-aae38e26833d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;alert-js&quot;)/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>a6be8744-e60c-4e22-baab-2b79808fce8e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//small[@id='alert-js']/div</value>
      <webElementGuid>0abfdc55-c88d-40c3-82d0-1b2cb46432ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='number'])[1]/following::div[1]</value>
      <webElementGuid>f2e784f8-9607-442f-8917-01ebd7282907</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='symbol'])[1]/following::div[1]</value>
      <webElementGuid>5f441776-d728-4f1a-9120-6476ca99c3a5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/preceding::div[1]</value>
      <webElementGuid>ed706295-4547-426f-a193-e424288ed1a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.'])[1]/preceding::div[9]</value>
      <webElementGuid>a7985f96-6ac5-43ba-9e41-c224bb7fb85b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่าน']/parent::*</value>
      <webElementGuid>3994b9ad-08ee-42d4-b429-4f12f1f74035</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small/div</value>
      <webElementGuid>b0ec7b19-ea42-46f7-92f5-540c98f0b720</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)&quot;) or . = concat(&quot;ไม่พบตัวอักษรอักขระพิเศษ ในรหัสผ่านใหม่ของท่านตัวอย่าง (! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)Not found symbol in your new passwordExample(! &quot; # $ % &amp;  &quot; , &quot;'&quot; , &quot;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)&quot;))]</value>
      <webElementGuid>251f423c-e65c-468f-84e9-0e6ca8514932</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
