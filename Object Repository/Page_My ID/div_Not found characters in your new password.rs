<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Not found characters in your new password</name>
   <tag></tag>
   <elementGuidId>27447261-8045-4304-9b5a-c3213bd63440</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='alert-js']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>284c6a60-27c9-4f29-9aff-1eb6898583f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>c161ea4d-7f11-4e95-a0fa-fcba3b1abb83</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password</value>
      <webElementGuid>307bbab2-bf54-408f-8d0b-8758f8a28da3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;alert-js&quot;)/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>c498b867-666e-490b-8913-6be52192a186</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//small[@id='alert-js']/div</value>
      <webElementGuid>f1a93253-62ad-4f64-bef4-24aa1f4f4c9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='number'])[1]/following::div[1]</value>
      <webElementGuid>42ad6ce3-184c-46d4-afc6-674fcaf02c1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='symbol'])[1]/following::div[1]</value>
      <webElementGuid>e0838329-768a-457d-b9e4-3aebefc43b17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/preceding::div[1]</value>
      <webElementGuid>7c093b40-ffc9-4dd7-97d3-36db6d2eca83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.'])[1]/preceding::div[8]</value>
      <webElementGuid>81ac0410-5938-4554-a389-9c943c03a785</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่าน']/parent::*</value>
      <webElementGuid>7002911d-a491-4099-93da-306fcd8040fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small/div</value>
      <webElementGuid>a5d5b6f9-b42e-4f93-b205-3a89406afb51</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password' or . = 'ไม่พบตัวอักษรภาษาอังกฤษ ในรหัสผ่านใหม่ของท่านNot found characters in your new password')]</value>
      <webElementGuid>ab0f1d1c-b851-472b-ab9c-e9d299613d58</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
