<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password incorrect</name>
   <tag></tag>
   <elementGuidId>aae36df7-b16b-4f01-87eb-248f3d2f0549</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/div/div/div/form/div[4]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.text-center</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>521af1d1-d811-43ac-b913-3d2b969022ee</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>6e7a0c2c-1ae5-4ab9-8537-69238cad9a8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    </value>
      <webElementGuid>3864e564-6863-4ad2-b6fa-467d24f77cd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>c52cc1fe-82fc-4721-bef6-4664190ca75a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>4929822d-7fc0-41b6-b96b-0c4ced5bcc0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>cbadc7f2-db02-4943-9d0f-4fd9a7258800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    </value>
      <webElementGuid>4d3758a8-b1f6-4572-80e3-647dc9152912</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>4ba2d4b0-c78f-441c-a549-4186c395be03</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c0f3e19a-1923-43c8-9513-fd959af880e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>23f032a9-4409-45fc-8c3e-7d40b3a0f11f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    </value>
      <webElementGuid>0888c56f-685c-48a5-91aa-6d5f930c0d70</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>6a8cfb33-fa55-4a75-af4c-d5ad251af31d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>2e3542fb-4869-4143-93af-3dc67d8d4555</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>b1766810-a92a-4125-a4ee-dd24de3a6354</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    </value>
      <webElementGuid>d7960768-982a-495e-9ab4-54d7e42f9148</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>946df9b9-9bf4-47c7-b27a-df411444bd5e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form/div[4]</value>
      <webElementGuid>aff2e283-a778-4fd1-bacb-06bfdd982602</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ลืมรหัสผ่าน / reset password'])[1]/following::div[1]</value>
      <webElementGuid>4458391e-ab7a-4528-9950-f4512b9ef670</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เข้าสู่ระบบ / Sign in'])[1]/following::div[1]</value>
      <webElementGuid>8e03d169-2148-44e3-82d4-174e3844bb0b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.'])[1]/preceding::div[8]</value>
      <webElementGuid>dac4db5c-5aa1-40c4-b9a0-2277f970ad38</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='รหัสผ่านไม่ถูกต้อง']/parent::*</value>
      <webElementGuid>72379c88-95c9-4d67-9248-bca49503c254</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]</value>
      <webElementGuid>71d986d7-2cb5-493f-b746-f2a6ff8111a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    ' or . = '
                        รหัสผ่านไม่ถูกต้อง
                        Password incorrect!
                    ')]</value>
      <webElementGuid>7580d5ee-c8de-4499-acd6-1e3e619d77e6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
