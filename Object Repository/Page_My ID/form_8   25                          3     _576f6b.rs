<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_8   25                          3     _576f6b</name>
   <tag></tag>
   <elementGuidId>c7f76049-d861-47dd-8d86-e2ed58984dd6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#form-change-pass</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-change-pass']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>186fd4fb-4ffe-44eb-affc-36d58cb2a329</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-change-pass</value>
      <webElementGuid>ecb3c34c-7d26-40a1-9709-38e3c7b0a8d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>1b45f3a3-5874-4a60-94f0-395f11c19c7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://myid.buu.ac.th/profile/process_chgpwd</value>
      <webElementGuid>54bc9b49-2e2e-44f1-a513-7ca58c4ac89a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            </value>
      <webElementGuid>b26b0762-b456-476b-b49e-f788196ca012</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-change-pass&quot;)</value>
      <webElementGuid>ef70ada7-0637-4b85-a0ad-bfb77df2d462</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>83044ffe-4ca9-4ce8-b694-2dc0423783a8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-change-pass</value>
      <webElementGuid>b5811721-be43-4fac-8aa3-4c919f941edd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>b90b4c26-7013-4d2e-81c8-761e49bbb7ca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://myid.buu.ac.th/profile/process_chgpwd</value>
      <webElementGuid>9876c948-2e77-495e-abc0-ef5d398ea4db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            </value>
      <webElementGuid>86da5de6-8768-4180-a580-dc9f3794239b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-change-pass&quot;)</value>
      <webElementGuid>2cbb1934-f12b-45d0-a2dd-54d83f8a09aa</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@id='form-change-pass']</value>
      <webElementGuid>2084ee80-34bf-46e6-85e7-93b54731b270</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form</value>
      <webElementGuid>bd73f375-4856-4f27-b403-21252af00f0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เปลี่ยนรหัสผ่าน (Change Password)'])[1]/following::form[1]</value>
      <webElementGuid>6a13712f-01b9-489a-a7c0-ab0ff068a672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>8f0fd134-3b9b-4870-9a56-98f80caea853</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[@id = 'form-change-pass' and (text() = '
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            ' or . = '
            	
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp;  ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                

                
                    Change Password
                
                
                
            ')]</value>
      <webElementGuid>6eca08fb-95d2-4828-9fd2-2227724b04b1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
