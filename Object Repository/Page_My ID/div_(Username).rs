<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Username)</name>
   <tag></tag>
   <elementGuidId>6c8e7d9c-8cf5-43ac-8111-1e8693639f43</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/div/div/div/form/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-group</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>dcf35daf-1a68-462a-95c4-a88ba1f07bfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-group</value>
      <webElementGuid>45b5ebd4-5819-4fcd-94f3-07b15b7ef3df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    บัญชีผู้ใช้ (Username)
                    
                </value>
      <webElementGuid>2eb10a0f-5b22-428d-a41f-a4031a429381</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;form-group&quot;]</value>
      <webElementGuid>47146b32-e36b-4eb5-a2ce-64c8ea4e8a36</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form/div</value>
      <webElementGuid>18855493-0b84-4cb0-8cd4-ebdb365dda87</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='อ่านเพิ่มเติม'])[1]/following::div[1]</value>
      <webElementGuid>b81e9a57-a9a6-4fa0-8c8c-f978775e1588</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
      <webElementGuid>551334a6-c74f-41aa-8144-bbff56e8cdfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    บัญชีผู้ใช้ (Username)
                    
                ' or . = '
                    บัญชีผู้ใช้ (Username)
                    
                ')]</value>
      <webElementGuid>b49233f8-bc93-4d7b-ab65-f08ae96881ac</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
