<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_(Change Password)                      _9db6b4</name>
   <tag></tag>
   <elementGuidId>d7dd5942-7733-4ad3-b367-689470447c88</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-12</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8627eee2-3f08-4081-bbe1-2c6386984b01</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12</value>
      <webElementGuid>a4cca947-a1c2-40ae-8913-8e7b563d25de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                	เปลี่ยนรหัสผ่าน (Change Password)


    
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    







    
        
            
                
                
                ความพึงพอใจภาพร่วมต่อการใช้ระบบเปลี่ยนรหัสผ่าน
Overall satisfaction



  
    
      ดีมาก
      excellent
    
      ดี
      good
    
      ปานกลางmiddling
    
      พอใช้
      not good
    
      ควรปรับปรุง
      bad
  
  
    
    
    
    
    
  



  ข้อเสนอแนะ
                    Comment
                    
                    
                        Send
                  
                
            
            
        
    

                    
                    
                </value>
      <webElementGuid>071f429d-23c9-49cc-abd7-429a5496fb27</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]</value>
      <webElementGuid>215fff5e-864c-4a92-90ef-246215ecaf0b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div</value>
      <webElementGuid>b9a6d582-ce9a-4497-90fa-0ec5add67f1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div</value>
      <webElementGuid>9da3124c-8943-4595-87cf-5a218c9729c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                	เปลี่ยนรหัสผ่าน (Change Password)


    
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    







    
        
            
                
                
                ความพึงพอใจภาพร่วมต่อการใช้ระบบเปลี่ยนรหัสผ่าน
Overall satisfaction



  
    
      ดีมาก
      excellent
    
      ดี
      good
    
      ปานกลางmiddling
    
      พอใช้
      not good
    
      ควรปรับปรุง
      bad
  
  
    
    
    
    
    
  



  ข้อเสนอแนะ
                    Comment
                    
                    
                        Send
                  
                
            
            
        
    

                    
                    
                ' or . = '
                	เปลี่ยนรหัสผ่าน (Change Password)


    
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    







    
        
            
                
                
                ความพึงพอใจภาพร่วมต่อการใช้ระบบเปลี่ยนรหัสผ่าน
Overall satisfaction



  
    
      ดีมาก
      excellent
    
      ดี
      good
    
      ปานกลางmiddling
    
      พอใช้
      not good
    
      ควรปรับปรุง
      bad
  
  
    
    
    
    
    
  



  ข้อเสนอแนะ
                    Comment
                    
                    
                        Send
                  
                
            
            
        
    

                    
                    
                ')]</value>
      <webElementGuid>55f44b64-e11b-4ff2-838a-3042ccb81c36</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
