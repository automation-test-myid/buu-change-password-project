<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Change password successfully</name>
   <tag></tag>
   <elementGuidId>8d3b38f2-d5ec-4f74-8ecb-d60959c34212</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page-wrapper']/div/div/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>fd953e52-875f-4aea-8082-b776a0e42ca6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success text-center</value>
      <webElementGuid>9052ed23-a6cc-403b-88a4-b3c11f55ae36</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    </value>
      <webElementGuid>1f563def-5db5-4338-9b7c-fc89faf61a6a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page-wrapper&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12&quot;]/div[1]/div[1]/div[@class=&quot;alert alert-success text-center&quot;]</value>
      <webElementGuid>ce111201-978e-4b5a-82da-530de61433ea</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div</value>
      <webElementGuid>dbdaf339-8e72-4982-a977-edff818abdcd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เปลี่ยนรหัสผ่าน (Change Password)'])[1]/following::div[3]</value>
      <webElementGuid>b12c1ef1-5d1e-446c-907e-c74dbc31033c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='เปลี่ยนรหัสผ่านสำเร็จ']/parent::*</value>
      <webElementGuid>1d070ca2-cf0b-48a7-bd8a-d6f44f95cc12</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div</value>
      <webElementGuid>b5750038-d038-407d-930c-cfb9a74c8b2a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    ' or . = '
       เปลี่ยนรหัสผ่านสำเร็จ
       
       Change password successfully.
    ')]</value>
      <webElementGuid>c591d04e-2af4-49df-bf13-0d48bf6aa335</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
