<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Not found number in your new password</name>
   <tag></tag>
   <elementGuidId>816ad43a-1103-4fbe-bdb7-81deee7ceefc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-danger.text-center</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//small[@id='alert-js']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f0ce3dc7-a735-4160-a815-9b03b5080cd6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-danger text-center</value>
      <webElementGuid>2c14b925-0967-414d-a017-c6a531098a29</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>ไม่พบตัวเลข ในรหัสผ่านใหม่ของท่านNot found number in your new password</value>
      <webElementGuid>532909ec-26f9-4120-a300-82c7d3e1fb7c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;alert-js&quot;)/div[@class=&quot;alert alert-danger text-center&quot;]</value>
      <webElementGuid>07fbf4ff-91ef-43af-9ace-a424a608b84b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//small[@id='alert-js']/div</value>
      <webElementGuid>a9d567a7-e1ee-43b6-928c-b7941c5497f7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='number'])[1]/following::div[1]</value>
      <webElementGuid>b317a9fc-3b79-445b-beff-23b1cda1f838</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='symbol'])[1]/following::div[1]</value>
      <webElementGuid>0e9d0cec-8d9c-4bd7-8f19-6a00e420c672</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/preceding::div[1]</value>
      <webElementGuid>99473d31-02b0-4ea2-b54d-5a34638ce9b8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='© 2018 COMPUTER CENTER BURAPHA UNIVERSITY ALL RIGHTS RESERVED.'])[1]/preceding::div[8]</value>
      <webElementGuid>5d2cfdf0-128f-4595-8d86-e45536cf9a35</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='ไม่พบตัวเลข ในรหัสผ่านใหม่ของท่าน']/parent::*</value>
      <webElementGuid>d725b60c-91a3-4a36-8c7a-1fbdf252b0bb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small/div</value>
      <webElementGuid>0268ff58-3500-440e-8dc8-24ce91cd0281</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'ไม่พบตัวเลข ในรหัสผ่านใหม่ของท่านNot found number in your new password' or . = 'ไม่พบตัวเลข ในรหัสผ่านใหม่ของท่านNot found number in your new password')]</value>
      <webElementGuid>43b3567e-a46a-4d2b-a50d-eefbe3cc2478</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
