<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>path (1)</name>
   <tag></tag>
   <elementGuidId>bda2caa6-90b4-4d68-babf-a597bc56bdcc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div._9q4i > div._a3x9 > div.oajrlxb2.gs1a9yip.g5ia77u1.mtkw9kbi.tlpljxtp.qensuy8j.ppp5ayq2.goun2846.ccm00jje.s44p3ltw.mk2mc5f4.rt8b4zig.n8ej3o3l.agehan2d.sk4xxmp2.rq0escxv.nhd2j8a9.mg4g778l.pfnyh3mw.p7hjln8o.kvgmc6g5.cxmmr5t8.oygrvhab.hcukyx3x.tgvbjcpo.hpfvmrgz.jb3vyjys.rz4wbd8a.qt6c0cv9.a8nywdso.l9j0dhe7.i1ao9s8h.esuyzwwr.f1sip0of.du4w35lb.n00je7tq.arfg74bv.qs9ysxi8.k77z8yql.pq6dq46d.btwxx1t3.abiwlrkh.p8dawk7l.lzcic4wl > svg > path</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>path</value>
      <webElementGuid>9c60e634-b088-49f2-8801-66b8f44aa65a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>d</name>
      <type>Main</type>
      <value>M0 16C0 7.16344 7.16344 0 16 0C24.8366 0 32 7.16344 32 16C32 24.8366 24.8366 32 16 32C7.16344 32 0 24.8366 0 16Z</value>
      <webElementGuid>61cff9f2-5e2a-4409-ae60-d82569da5126</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill</name>
      <type>Main</type>
      <value>black</value>
      <webElementGuid>437b937f-88ed-4439-a396-9bfada8c9634</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>fill-opacity</name>
      <type>Main</type>
      <value>0.04</value>
      <webElementGuid>77f1ec6c-60c4-4d44-a8d7-b92ad89dde18</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;u_0_0_08&quot;)/div[1]/div[1]/div[@class=&quot;_94ve&quot;]/div[@class=&quot;_94vf _9pmv&quot;]/div[1]/div[@class=&quot;welcomePageModalSheetContent&quot;]/div[@class=&quot;_a2zp&quot;]/div[@class=&quot;_9dzn&quot;]/div[@class=&quot;_9q4i&quot;]/div[@class=&quot;_a3x9&quot;]/div[@class=&quot;oajrlxb2 gs1a9yip g5ia77u1 mtkw9kbi tlpljxtp qensuy8j ppp5ayq2 goun2846 ccm00jje s44p3ltw mk2mc5f4 rt8b4zig n8ej3o3l agehan2d sk4xxmp2 rq0escxv nhd2j8a9 mg4g778l pfnyh3mw p7hjln8o kvgmc6g5 cxmmr5t8 oygrvhab hcukyx3x tgvbjcpo hpfvmrgz jb3vyjys rz4wbd8a qt6c0cv9 a8nywdso l9j0dhe7 i1ao9s8h esuyzwwr f1sip0of du4w35lb n00je7tq arfg74bv qs9ysxi8 k77z8yql pq6dq46d btwxx1t3 abiwlrkh p8dawk7l lzcic4wl&quot;]/svg[1]/path[1]</value>
      <webElementGuid>ca0f9d45-35e9-437c-b342-6224d9001770</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Page_My ID/iframe_Change Password_f362e88b9ceec04</value>
      <webElementGuid>5f33bb12-741f-40ec-bb43-8b37919f3665</webElementGuid>
   </webElementProperties>
</WebElementEntity>
