<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>form_minimum of 8</name>
   <tag></tag>
   <elementGuidId>5d0596db-b9e0-4c8d-bd7a-1a98db4d4a55</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='form-change-pass']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#form-change-pass</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>form</value>
      <webElementGuid>e6292341-d190-4488-b5a8-f42142338984</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>form-change-pass</value>
      <webElementGuid>7d65b292-750e-4869-a552-d72bbad03173</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>method</name>
      <type>Main</type>
      <value>post</value>
      <webElementGuid>7b1e6db2-3a90-4834-b7f8-bfef97574e07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>action</name>
      <type>Main</type>
      <value>https://myid.buu.ac.th/ad/changepwd</value>
      <webElementGuid>cbcaeac3-efaa-4023-a77f-03ec25c0c17f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            </value>
      <webElementGuid>d2b3dc46-d044-46f2-a9fc-b86230f2b807</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;form-change-pass&quot;)</value>
      <webElementGuid>5b1b256f-8b6c-4d94-b1a1-74baf2372953</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//form[@id='form-change-pass']</value>
      <webElementGuid>2780579a-8e8a-4838-936c-75230cf583eb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page-wrapper']/div/div/div/div/div/form</value>
      <webElementGuid>7b0ed0d3-3548-41c4-a08f-f7ff9a5cb471</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='เปลี่ยนรหัสผ่าน (Change Password)'])[1]/following::form[1]</value>
      <webElementGuid>59525013-dd51-442b-8336-49bba22a728e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form</value>
      <webElementGuid>fc845518-a520-4e4d-855e-c48eadc487d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//form[@id = 'form-change-pass' and (text() = '
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            ' or . = '
                
                        
                  คำแนะนำ
                             รหัสผ่านต้องไม่น้อยกว่า 8 ตัวอักษร แต่ไม่เกิน 25 ตัวอักษร
                             ห้ามตั้งรหัสผ่านซ้ำกับรหัสผ่านเดิม
                             รหัสผ่านต้องประกอบด้วย 3 สิ่ง ดังต่อไปนี้
                                  - ตัวอักษรภาษาอังกฤษตัวใหญ่หรือเล็ก (A-Z, a-z)
                                  - ตัวเลข (0-9)
                                  - อักขระพิเศษ (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                        
                        [Password Rules and Requirements]
                             Your password must contain ALL of the following:
                                  - Do not use the same password as the old password.
                                  - A minimum of 8 characters (maximum 25)
                                  - Contain at least 1 uppercase or lowercase letter (A-Z, a-z)
                                  - Contain at least 1 number (0-9)
                          - Contain at least 1 symbol (! &quot; # $ % &amp; ( ) * + , - . / : ; &lt; = > ? @ [ ] ^ _ ` { | } ~)
                
                
                    บัญชีผู้ใช้ (Username)
                    
                    
                    
                
                
                    รหัสผ่านเก่า (Old Password)
                    
                    
                
                
                
                    รหัสผ่านใหม่ (New Password)
                    
                    
                
                
                    รหัสผ่านใหม่อีกครั้ง (Re-New Password)
                    
                    
				Show Password
                
                
                  Password must contain the following (รหัสผ่านประกอบไปด้วย) :
                  A lowercase or uppercase letter (ตัวอักษรตัวเล็กหรือตัวใหญ่)
                  A symbol letter (อักขระพิเศษ)
                  A number (ตัวเลข)
                  Minimum 8 characters (ขั้นต่ำ 8 ตัว)
                
                
                    
                
                
                    Change Password
                
            ')]</value>
      <webElementGuid>fde2a4e1-1c39-4fdd-a4d7-c99f803ff184</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
